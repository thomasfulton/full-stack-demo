const ratesRepository = require("../repository/rates");

function filterSingles([snaptravelData, retailData]) {
  const map = {};

  snaptravelData.forEach(item => {
    map[item.id] = item.price;
  });

  let output = [];

  retailData.forEach(item => {
    const snaptravelPrice = map[item.id];
    if (snaptravelPrice !== undefined) {
      output.push(Object.assign(item, { snaptravelPrice }));
    }
  });

  return output;
}

function post(req, res, next) {
  const { city, checkin, checkout } = req.body;

  Promise.all([
    ratesRepository.getRates(city, checkin, checkout, "snaptravel"),
    ratesRepository.getRates(city, checkin, checkout, "retail"),
  ])
    .then(filterSingles)
    .then(searchResults => {
      console.log("Search results:", searchResults);
      return searchResults;
    })
    .then(searchResults => res.render("search-results", { searchResults }))
    .catch(next);
}

module.exports = { post };
