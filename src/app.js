const express = require("express");
const bodyParser = require("body-parser");

const router = require("./router");
const app = express();
const port = 3000;

app.set("view engine", "pug");
app.set("views", "./src/views");

app.use(bodyParser.urlencoded({ extended: true })); 
app.use(router);

app.listen(port, () =>
  console.log(`Snaptravel Demo App :: Listening on port ${port}`)
);
