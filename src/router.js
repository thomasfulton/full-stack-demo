const express = require("express");
const searchController = require("./controller/search");
const indexController = require("./controller/index");

const router = express.Router();

router.route("/").get(indexController.get);
router.route("/search").post(searchController.post);

module.exports = router;
