const axios = require("axios");
const ratesApiBaseUrl = require("config").get("ratesApiBaseUrl");

// This is a terrible way to cache in prod but there's
// not enough time to set up Redis or similar. At the very
// least you would want an LRU cache if you're going to go with
// in-memory caching so you don't run out of memory.
let cache = {};

function generateCacheKey(city, checkin, checkout, provider) {
  return `${city}:${checkin}:${checkout}:${provider}`;
}

async function getRates(city, checkin, checkout, provider) {
  const cacheKey = generateCacheKey(city, checkin, checkout, provider);
  const cacheItem = cache[cacheKey];
  if (cacheItem) {
    console.log(`Cache hit for ${cacheKey}`);
    return cacheItem;
  }
  console.log(`Cache miss for ${cacheKey}`);
  return axios
    .post(`${ratesApiBaseUrl}/interview/hotels`, {
      city,
      checkin,
      checkout,
      provider,
    })
    .then(response => {
      const hotelsArray = response.data.hotels;
      console.log("Rates api response:", hotelsArray);
      cache[cacheKey] = hotelsArray;
      return hotelsArray;
    });
}

module.exports = { getRates };
